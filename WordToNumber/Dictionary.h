#pragma once
#include<string>
#include<map>

struct Tokens {
	std::map<std::string, int> singleDigitTokens;
	std::map<std::string, int> twoDigitTokens;
	std::map<std::string, int> secondDigitTokens;
	std::map<std::string, int> multipliers;
};

class Dictionary
{

public: 
	Tokens createEnglishDictionary();
	//std::tuple<std::map<std::string, int>, std::map<std::string, int>> createSpanishDictionary();

public:
	//std::map<std::string, std::map<std::string, int> > createDictionaries();

};

