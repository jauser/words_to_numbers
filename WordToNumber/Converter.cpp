#include "Converter.h"


Converter::Converter()
{
	this->language = "English";
	Dictionary dict = Dictionary();
	this->tokens = dict.createEnglishDictionary();
}

Converter::Converter(std::string language)
{
	this->language = language;
	Dictionary dict = Dictionary();
	this->tokens = dict.createEnglishDictionary();

};

Converter::Converter(Tokens tokens)
{
	this->tokens = tokens;

}



std::string Converter::getLanguage()
{
	return language;
};

std::string Converter::getNumber(const std::vector<std::string>& numParts)
{

	std::string result = "";
	int zero_digits;
	int tmp_value = 0;
	bool lastItemMultiplier = false;
	bool hasToken = false;
	bool hasSecondToken = false;
	bool hasFullToken = false;
	std::vector<std::string>::const_reverse_iterator iterator = numParts.rbegin();

	if (tokens.multipliers.count(*iterator)) {
		addZeroDigits(result, tokens.multipliers[*iterator]);
		lastItemMultiplier = true;
	}

	for (iterator; iterator != numParts.rend(); ++iterator) {

		if (tokens.singleDigitTokens.count(*iterator) != 0 ) {
			if (hasToken) {
				addValue(result, tmp_value);
				tmp_value = 0;
			}
			tmp_value += tokens.singleDigitTokens[*iterator];
			hasToken = true;
			lastItemMultiplier = false;
		}
		else if ( tokens.twoDigitTokens.count(*iterator) != 0) {
			if (hasToken) {
				addValue(result, tmp_value);
				tmp_value = 0;
			}
			tmp_value += tokens.twoDigitTokens[*iterator];
			hasToken = true;
			lastItemMultiplier = false;
		}
		else if (tokens.secondDigitTokens.count(*iterator) != 0 ) {
			if (hasSecondToken) {
				addValue(result, tmp_value);
				tmp_value = 0;
			}
			tmp_value += tokens.secondDigitTokens[*iterator];
			hasToken = true;
			hasSecondToken = true;
			lastItemMultiplier = false;
		}

		else if (tokens.multipliers.count(*iterator) != 0) {

			if (hasToken || hasSecondToken ) {
				addValue(result, tmp_value);
				hasToken = false;
				hasSecondToken = false;
			}
			/*
			if (*iterator == "hundred")
			{
				zero_digits = multipliers[*iterator] - result.length() % 3;
			}
			else {
				zero_digits = multipliers[*iterator] - result.length();
			}*/
			zero_digits = tokens.multipliers[*iterator];
			zero_digits -= (*iterator == "hundred") ? result.length() % 3 : result.length();

			addZeroDigits(result, zero_digits);
			tmp_value = 0;
			lastItemMultiplier = true;
		}
		else {
			addDigit(result, "?");
		}
	}
	if (hasToken || hasSecondToken) {
		addValue(result, tmp_value);
		hasToken = false;
		hasSecondToken = false;
	}
	else if (lastItemMultiplier == true) {
		addValue(result, 1);
	}


	return result;
}

void Converter::addZeroDigits(std::string& result, int quantity)
{
	for (int i = 0; i < quantity; ++i) {
		result = "0" + result;
	}
}

void Converter::addDigit(std::string& result, std::string digit)
{
	result = digit + result;
}

void Converter::addValue(std::string& result, int value)
{
	result = std::to_string(value) + result;
}

