#pragma once
#include "Dictionary.h"


#include<string>
#include<vector>
#include<map>

class Converter
{
private:
	std::string language;
	Tokens tokens;
	//std::map<std::string, int> tokens;
	std::map<std::string, int> multipliers;


public:
	Converter();
	Converter(std::string language);
	Converter(Tokens tokens);
	std::string getLanguage();
	std::string getNumber(const std::vector<std::string>& numParts);


private:
	void addZeroDigits(std::string& result, int quantity);
	void addDigit(std::string& result, std::string digit);
	void addValue(std::string& result, int value);
};

