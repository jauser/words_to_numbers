#include "Parser.h"

Parser::Parser() {
    this->language = "English";
    Dictionary dict = Dictionary();
    this->tokens = dict.createEnglishDictionary();
    converter = Converter(this->tokens);
}


Parser::Parser(std::string language) {
    this->language = language;
    Dictionary dict = Dictionary();
    this->tokens = dict.createEnglishDictionary();
    converter = Converter(this->tokens);

};

std::string Parser::getLanguage() {
    return language;
}


std::string Parser::computeNumbers(std::string text)
{   
    std::string result = "";
    std::vector<std::string> words = split_words(text);


    bool readingNumber = false;
    std::vector<std::string> number;

    for (std::vector<std::string>::const_iterator iterator = words.begin(); iterator != words.end(); ++iterator) {
        if (isNumberPart(*iterator)) {
            if (!readingNumber) readingNumber = true;
            number.push_back(*iterator);
        }
        else {
            if (readingNumber) {

                result += converter.getNumber(number) + " ";
                readingNumber = false;
                number.empty();
            }
            result += *iterator + " ";
        }
    }
    return result.substr(0, result.size() - 1);

}


std::vector<std::string> Parser::split_words(std::string text)
{
    std::istringstream ss(text);
    std::vector<std::string> results(std::istream_iterator<std::string>{ss},
        std::istream_iterator<std::string>());

    return results;
}

bool Parser::isNumberPart(std::string word)
{
    converter = Converter(this->tokens);
    if (tokens.multipliers.count(word) || tokens.singleDigitTokens.count(word) || tokens.twoDigitTokens.count(word) || tokens.secondDigitTokens.count(word)) {
        return true;
    }
    return false;
}
;


