#pragma once
#include "Converter.h"
#include "Dictionary.h"

#include <string>
#include <sstream>

#include <map>
#include <iterator> 
#include <vector> 




class Parser
{
    Converter converter;
    std::string language;
    Tokens tokens;
    //std::map<std::string, int> tokens;
    std::map<std::string, int> multipliers;

public:

    Parser();

    Parser(std::string language);
    std::string getLanguage();

    std::string computeNumbers(std::string text);

private:
    std::string split_paragraphs(std::string text);
    std::vector<std::string> split_words(std::string text);

    bool isNumberPart(std::string word);
};


/*
class EnglishParser : public Converter {

};*/

