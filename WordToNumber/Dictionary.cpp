#include "Dictionary.h"


#include <string>
#include <map>



Tokens  Dictionary::createEnglishDictionary() {
    
    Tokens tokens = Tokens();
    //std::map<std::string, int> singleDigitTokens;
    std::string ones[] = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
    //std::map<std::string, int> twoDigitTokens;
    std::string teens[] = { "ten", "eleven", "twelve", "therteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
    //std::map<std::string, int> secondDigitTokens;
    std::string tens[] = { "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
    //std::map<std::string, int> multipliers;
    std::string multipliers_text[] = { "hundred", "thousand", "million" };
    int multipliers_values[] = { 2, 3, 6 };
    for (int i = 0; i < 10; i++) {
        tokens.singleDigitTokens[ones[i]] = i;
    }
    for (int i = 0; i < 10; i++) {
        tokens.twoDigitTokens[teens[i]] = i + 10;
    }
    for (int i = 2; i < 10; i++) {
        tokens.secondDigitTokens[tens[i-2]] = i * 10;
    }
    for (int i = 0; i < 3; i++) {
        tokens.multipliers[multipliers_text[i]] = multipliers_values[i];
    }
    return tokens;
}



/*
std::tuple<std::map<std::string, int>, std::map<std::string, int>> Dictionary::createSpanishDictionary() {

    std::string ones[] = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
    std::string teens[] = { "ten", "eleven", "twelve", "therteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
    std::string tens[] = { "ten", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety" };
    std::string multipliers[] = { "hundred", "thousand", "million" };
    int multipliers_values[] = { 100, 1000, 1000000 };

    std::map<std::string, int> spanishMap;

    for (int i = 0; i < 10; i++) {
        spanishMap[ones[i]] = i;
    }
    for (int i = 0; i < 10; i++) {
        spanishMap[teens[i]] = i + 10;
    }
    for (int i = 0; i < 11; i++) {
        spanishMap[tens[i]] = i * 10;
    }
    for (int i = 0; i < 11; i++) {
        spanishMap[multipliers[i]] = multipliers_values[i];
    }
    return spanishMap;
}


std::map<std::string, std::map<std::string, int> > Dictionary::createDictionaries() {
    std::map <std::string, std::map<std::string, int> > languagesMap;

    languagesMap["English"] = createEnglishDictionary();
    languagesMap["Spanish"] = createSpanishDictionary();

    return languagesMap;
}*/