#include "pch.h"
#include "CppUnitTest.h"

//#include "../WordToNumber/Reader.h"
#include "../WordToNumber/Converter.cpp"
//#include "../WordToNumber/Parser.h"
#include "../WordToNumber/Parser.cpp"
//#include "../WordToNumber/Dictionary.h"
#include "../WordToNumber/Dictionary.cpp"

#include <string>
using namespace Microsoft::VisualStudio::CppUnitTestFramework;


namespace UnitTest
{
	TEST_CLASS(ParserUnitTest)
	{
	public:
		
		TEST_METHOD(ParserLoad)
		{
			std::string language = "English";
			Parser parser = Parser(language);
			Assert::AreEqual(language, parser.getLanguage());
		}

		TEST_METHOD(Parser_T1)
		{
			std::string language = "English";
			Parser parser = Parser(language);

			std::string input = "there are one hundred one apples!";
			std::string output = "there are 101 apples!";
			Assert::AreEqual(output, parser.computeNumbers(input));
		}
	};


	TEST_CLASS(ReaderUnitTest)
	{
	public:

		TEST_METHOD(ReaderLoad)
		{
			std::string language = "English";
			Converter reader = Converter(language);
			Assert::AreEqual(language, reader.getLanguage());
		}

		TEST_METHOD(Reader_0)
		{
			std::string result = "0";
			std::vector<std::string> question;
			question.push_back("zero");

			std::string language = "English";
			Converter reader = Converter(language);
			Assert::AreEqual(result, reader.getNumber(question));
		}
		TEST_METHOD(Reader_1)
		{
			std::string result = "1";
			std::vector<std::string> question;
			question.push_back("one");

			std::string language = "English";
			Converter reader = Converter(language);
			Assert::AreEqual(result, reader.getNumber(question));
		}

		TEST_METHOD(Reader_2)
		{
			std::string result = "2";
			std::vector<std::string> question;
			question.push_back("two");

			std::string language = "English";
			Converter reader = Converter(language);
			Assert::AreEqual(result, reader.getNumber(question));
		}

		TEST_METHOD(Reader_3)
		{
			std::string result = "3";
			std::vector<std::string> question;
			question.push_back("three");

			std::string language = "English";
			Converter reader = Converter(language);
			Assert::AreEqual(result, reader.getNumber(question));
		}

		TEST_METHOD(Reader_4)
		{
			std::string result = "4";
			std::vector<std::string> question;
			question.push_back("four");

			std::string language = "English";
			Converter reader = Converter(language);
			Assert::AreEqual(result, reader.getNumber(question));
		}

		TEST_METHOD(Reader_41)
		{
			std::string result = "42";
			std::vector<std::string> question;
			question.push_back("forty");
			question.push_back("two");

			std::string language = "English";
			Converter reader = Converter(language);
			Assert::AreEqual(result, reader.getNumber(question));
		}


		TEST_METHOD(Reader_105)
		{
			std::string result = "105";
			std::vector<std::string> question;
			question.push_back("one");
			question.push_back("hundred");
			question.push_back("five");

			std::string language = "English";
			Converter reader = Converter(language);
			Assert::AreEqual(result, reader.getNumber(question));
		}

		TEST_METHOD(Reader_105_noONE)
		{
			std::string result = "105";
			std::vector<std::string> question;
			question.push_back("hundred");
			question.push_back("five");

			std::string language = "English";
			Converter reader = Converter(language);
			Assert::AreEqual(result, reader.getNumber(question));
		}

		TEST_METHOD(Reader_1000)
		{
			std::string result = "1000";
			std::vector<std::string> question;
			question.push_back("thousand");

			std::string language = "English";
			Converter reader = Converter(language);
			Assert::AreEqual(result, reader.getNumber(question));
		}


		TEST_METHOD(Reader_547359)
		{
			std::string result = "547359";
			std::vector<std::string> question;
			question.push_back("five");
			question.push_back("hundred");
			question.push_back("forty");
			question.push_back("seven");
			question.push_back("thousand");
			question.push_back("three");
			question.push_back("hundred");
			question.push_back("fifty");
			question.push_back("nine");

			std::string language = "English";
			Converter reader = Converter(language);
			Assert::AreEqual(result, reader.getNumber(question));
		}


		TEST_METHOD(Reader_354100542)
		{
			std::string result = "354100542";
			std::vector<std::string> question;
			question.push_back("three");
			question.push_back("hundred");
			question.push_back("fifty");
			question.push_back("four");
			question.push_back("million");
			question.push_back("one");
			question.push_back("hundred");
			question.push_back("thousand");
			question.push_back("five");
			question.push_back("hundred");
			question.push_back("forty");
			question.push_back("two");

			std::string language = "English";
			Converter converter = Converter(language);
			Assert::AreEqual(result, converter.getNumber(question));
		}

		TEST_METHOD(Reader_Year_1982)
		{
			std::string result = "1982";
			std::vector<std::string> question;
			question.push_back("nineteen");
			question.push_back("eighty");
			question.push_back("two");

			std::string language = "English";
			Converter converter = Converter(language);
			Assert::AreEqual(result, converter.getNumber(question));
		}

		TEST_METHOD(Reader_Year_1604)
		{
			std::string result = "1604";
			std::vector<std::string> question;
			question.push_back("sixteen");
			question.push_back("zero");
			question.push_back("four");


			std::string language = "English";
			Converter converter = Converter(language);
			Assert::AreEqual(result, converter.getNumber(question));
		}
		TEST_METHOD(Reader_PhoneNumber_758234861)
		{
			std::string result = "758234861";
			std::vector<std::string> question;
			question.push_back("seven");
			question.push_back("five");
			question.push_back("eight");
			question.push_back("two");
			question.push_back("three");
			question.push_back("four");
			question.push_back("eight");
			question.push_back("six");
			question.push_back("one");

			std::string language = "English";
			Converter converter = Converter(language);
			Assert::AreEqual(result, converter.getNumber(question));
		}

		TEST_METHOD(Reader_Year_372_8)
		{
			std::string result = "372";
			std::vector<std::string> question;
			question.push_back("three");
			question.push_back("hundred");
			question.push_back("seventy");
			question.push_back("two");

			std::string language = "English";
			Converter converter = Converter(language);
			Assert::AreEqual(result, converter.getNumber(question));
		}

		TEST_METHOD(Reader_Year_372_9)
		{
			std::string result = "372";
			std::vector<std::string> question;
			question.push_back("three");
			question.push_back("hundred");
			question.push_back("seventy");
			question.push_back("two");

			std::string language = "English";
			Converter converter = Converter(language);
			Assert::AreEqual(result, converter.getNumber(question));
		}
	};
}
